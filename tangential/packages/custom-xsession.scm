(define-module (tangential packages custom-xsession)
  #:use-module (guix packages)
  #:use-module (guix build-system trivial)
  #:use-module (guix git-download)
  #:use-module (guix licenses))

(define-public custom-xsession
  (package
    (name "custom-xsession")
    (version "0.1.0")
    (source (origin
	      (method git-fetch)
	      (sha256
		(base32 "1ckw1pa52vrncb0wy8bh5rxd2ikqykwpv95k7v1lh3dg2l3x3hs7"))
	      (uri (git-reference
		     (url "https://gitlab.com/tangential/guix-pkg-custom-xsession.git")
		     (commit "846416b1")))))
    (build-system trivial-build-system)
    (arguments
      `(#:modules ((guix build utils))
	#:builder (begin
	  (use-modules (guix build utils))
          (let
            ((xsessions (string-append %output "/share/xsessions")))
            (mkdir-p xsessions)
            (call-with-output-file
              (string-append xsessions "/xsession.desktop")
              (lambda (port)
                (format port "~
                  [Desktop Entry]~@
                  Name=Custom user .xsession~@
                  Comment=Launches a user's custom .xsession~@
                  Exec=~~/.xsession~@
                  Type=Application~%")))))))
    (synopsis "Support ~/.xsession scripts")
    (description "Adds a .desktop X11 session definition for launching custom user defined WMs")
    (home-page "https://gitlab.com/tangential/guix-pkg-custom-xsession")
    (license expat)))